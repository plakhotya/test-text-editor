import axios from 'axios'
const api_url = "http://api.datamuse.com/words"
const syn_key = "rel_syn"

export default function getSynonyms(word){
  return axios.get(`${api_url}?${syn_key}=${word}`)
}