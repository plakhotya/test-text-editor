export default [
  {
    title: "Bold",
    command: "bold",
    shortTitle: "B",
    css: "font-weight: bold;"
  },{
    title: "Italic",
    command: "italic",
    shortTitle: "I",
    css: "font-style: italic;"
  },{
    title: "Underline",
    command: "underline",
    shortTitle: "U",
    css: "text-decoration: underline;"
  }
]